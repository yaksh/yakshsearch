
package test.edu.upenn.cis455;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import edu.upenn.cis455.QueryEngine.FastQueryParser;
import edu.upenn.cis455.QueryEngine.NLPQueryParser;
import edu.upenn.cis455.QueryEngine.QueryParser;
import edu.upenn.cis455.WebCrawler.FrontierList;
import junit.framework.TestCase;

/**
 * @author Jatin Sharma
 *
 */
public class TestFrontierList extends TestCase{

	@Test
	public void testAddRemoveSize() { 
		
		FrontierList<String> frontier = new FrontierList<String>();

		frontier.add("1");
		frontier.add("2");
		frontier.add("3");
		frontier.remove();
	
		assertEquals(frontier.size(), 2);
	}
	
	@Test
	public void testWriteToFile() { 


		FrontierList<String> frontier = new FrontierList<String>();

		List<String> objectList = new ArrayList<String>();
		objectList.add("4");
		objectList.add("5");
		frontier.addAll(objectList);
		
		frontier.writeToFile("models/frontierListDemo.txt");
		frontier.clear();

		// assert not null
		assertEquals(new File("models/frontierListDemo.txt").exists(), true);	

	}
	
	@Test
	public void testClear() { 

		FrontierList<String> frontier = new FrontierList<String>();

		List<String> objectList = new ArrayList<String>();
		objectList.add("4");
		objectList.add("5");
		frontier.addAll(objectList);

		frontier.clear();

		assertEquals(frontier.size(), 0);

	}
	
	
}
