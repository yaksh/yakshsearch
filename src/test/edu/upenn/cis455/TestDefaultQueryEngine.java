
package test.edu.upenn.cis455;

import java.util.List;

import org.junit.Test;

import edu.upenn.cis455.QueryEngine.FastQueryParser;
import edu.upenn.cis455.QueryEngine.NLPQueryParser;
import edu.upenn.cis455.QueryEngine.QueryParser;
import junit.framework.TestCase;

/**
 * @author Jatin Sharma
 *
 */
public class TestDefaultQueryEngine extends TestCase{

	@Test
	public void testFastQueryParser() { 
		
		// Get parameters
		String searchQuery = "How to knot a tie.";
		QueryParser queryParser = new FastQueryParser();
		String[] keywordList = queryParser.getKeywords(searchQuery);
		
		// assert not null
		assertNotNull(keywordList);	
	}
	
	@Test
	public void testNLPQueryParser() { 
		
		// Get parameters
		String searchQuery = "How to knot a tie.";
		QueryParser queryParser = new NLPQueryParser();
		String[] keywordList = queryParser.getKeywords(searchQuery);
		
		assertNotNull(keywordList);	
	}
	
}
