package edu.upenn.cis455.SearchEngine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.LinkedList;

// Test Comment - Swapnil

/**
 * @author cis455
 *
 */
public class BTree implements Serializable{
	
	private Hashtable<String, Leaf> BTreeMap;
	private static int numBuckets = 50000;
	private String directoryPath;

	public BTree(String directoryPath) {
		if(directoryPath!=null && directoryPath != ""){
			if(!directoryPath.endsWith("/"))
				directoryPath+="/";		
		}
		this.directoryPath = directoryPath;
		BTreeMap = new Hashtable<String, Leaf>(this.numBuckets);
	}

	/**
	 * @return the bTreeMap
	 */
	public Hashtable<String, Leaf> getBTreeMap() {
		return BTreeMap;
	}


	public void writeToFile(String filePath) {

		File fout = new File(filePath);
		FileOutputStream fos;
		ObjectOutputStream oos = null;
		try {
			// write object to file
			fos = new FileOutputStream(fout);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(this);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void readFromFile(String filePath) {

		File fin = new File(filePath);
		FileInputStream fis;
		ObjectInputStream ois= null;
		BTree bTree = null;
		try {
			// read object from file
			fis = new FileInputStream(fin);
			ois = new ObjectInputStream(fis);
			bTree = (BTree) ois.readObject();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ois.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		this.BTreeMap = bTree.BTreeMap;
		this.directoryPath = bTree.directoryPath;
	}

	public void add(String filePath) {
		
		File fin = new File(filePath);
		FileInputStream fis;
		BufferedReader br = null;
		
		File fout;
		FileOutputStream fos;
		BufferedWriter bw = null;
		try {
			fis = new FileInputStream(fin);
			br = new BufferedReader(new InputStreamReader(fis));

			// word	<tab> url1;;0,3,5||url4;;20,31,57||url57;;0,3,5
			String line = null;
			String word;
			String cacheUrlString;
			String[] keyVal;
			String[] urlWordFreqArray;
			String[] urlWordFreq;
			String url;
			String[] wordLocationsStrings;
			ArrayList<Integer> wordLocations = new ArrayList<Integer>();
			ArrayList<CacheUrl> cacheUrlList;
			StringBuffer UrlBuffer;
			Leaf leaf;
			while((line = br.readLine()) != null){
				// each line is one leaf
				// word	<tab> url1;;0,3,5||url4;;20,31,57||url57;;0,3,5

				try{
					if(line.contains("\t")){
						keyVal = line.split("\t");
						word = keyVal[0];
						cacheUrlString = keyVal[1];
						System.out.println("("+word+", "+cacheUrlString+")");
						
						// create a leaf and add to BTree
						cacheUrlList = new ArrayList<CacheUrl>();
						
						// what if only one URL is there. No pipes. [e.g. word <tab> url1;;0,4,5]
						UrlBuffer = new StringBuffer();
						if(cacheUrlString.contains("||")){
							urlWordFreqArray = cacheUrlString.split("\\|\\|");
							if(urlWordFreqArray != null && urlWordFreqArray.length > 0){
								for(int i=0; i<urlWordFreqArray.length; i++){
									urlWordFreq = urlWordFreqArray[i].split(";;");
									url = urlWordFreq[0];
									wordLocationsStrings = urlWordFreq[1].split(",");
									for (int k=0; k<wordLocationsStrings.length; k++){
										Integer loc = Integer.parseInt(wordLocationsStrings[k].trim());
										wordLocations.add(loc);
									}
									
									//save in BTree (PR- not known yet)
									double urlPR = 0.0;
									UrlBuffer.append(url+"\t"+urlPR+"\n");
									cacheUrlList.add(new CacheUrl(
											url, 
											urlPR, 
											wordLocationsStrings.length, 
											wordLocations));
								}
							}
						}
						leaf = new Leaf(this.directoryPath+word+".txt", cacheUrlList);
						this.BTreeMap.put(word, leaf);
						//write to file
						fout = new File(this.directoryPath+word+".txt");
						fos = new FileOutputStream(fout);
						bw = new BufferedWriter(new OutputStreamWriter(fos));
						bw.write(UrlBuffer.toString());
						bw.close();
					}
				}catch(Exception ex){
					System.err.println("Error parsing the word file");
					ex.printStackTrace();
				}
				
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public ArrayList<CacheUrl> get(String word){
		return this.BTreeMap.get(word).getCacheUrlList();
	}
	
	public static void main(String[] args) {

		BTree bTree = new BTree("/home/cis455/workspace/BTreeStore");
		bTree.add("/home/cis455/workspace/IndexedStore/test.txt");

		bTree.writeToFile("/home/cis455/workspace/BTreeStore/BTreeCache.ser");
		Leaf appleLeaf = bTree.BTreeMap.get("apple");
		System.out.println(appleLeaf.getFilePath());
		System.out.println(appleLeaf.getCacheUrlList().size());
		
		
		BTree bTreeNew = new BTree("/home/cis455/workspace/BTreeStore2");
		bTreeNew.readFromFile("/home/cis455/workspace/BTreeStore/BTreeCache.ser");
		
		appleLeaf = bTreeNew.BTreeMap.get("apple");
		System.out.println(appleLeaf.getFilePath());
		System.out.println(appleLeaf.getCacheUrlList().size());
		
		//////////////////// for Swapnil ///////////////////
		
		// One time setup
		BTree btree = new BTree("/home/cis455/workspace/BTreeStore2");
		btree.readFromFile("/home/cis455/workspace/BTreeStore/BTreeCache.ser");
		
		// for every keyword
		String keyword = "apple";
		ArrayList<CacheUrl> urlList = btree.get(keyword);
		System.out.println("Found "+ urlList.size() + " urls for "+ keyword);
		System.out.println("URL\tPR\tTF");
		for(CacheUrl url : urlList){
			System.out.println(url.toString()+"\t"+url.getPR()+"\t"+url.getTF());
		}
		
		// merge and other operations as per logic
		
		
	}
}

class Leaf implements Serializable{
	private String filePath;
	private ArrayList<CacheUrl> cacheUrlList;
	
	public Leaf(String filePath, ArrayList<CacheUrl> cacheUrlList){
		this.filePath = filePath;
		this.cacheUrlList = cacheUrlList;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath
	 *            the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return the cacheUrlList
	 */
	public ArrayList<CacheUrl> getCacheUrlList() {
		return cacheUrlList;
	}

	/**
	 * @param cacheUrlList
	 *            the cacheUrlList to set
	 */
	public void setCacheUrlList(ArrayList<CacheUrl> cacheUrlList) {
		this.cacheUrlList = cacheUrlList;
	}
}

