/**
 * 
 */
package edu.upenn.cis455.SearchEngine;

/**
 * @author cis455
 *
 */
public class TextSimilarity {
	
	private LevensteinDistance levenstein;
	private NGramDistance nGram;
	private JaroWinklerDistance jaroWinkler;
	
	public TextSimilarity(){
		this.levenstein = new LevensteinDistance();
		this.nGram = new NGramDistance();
		this.jaroWinkler = new JaroWinklerDistance();
	}

	public float getSimilarity(String query, String targetString){
		
		float levensteinSimilarity= levenstein.getDistance(query, targetString);
		float ngramSimilarity = nGram.getDistance(query, targetString);
		float jaroWinklerSimilarity = jaroWinkler.getDistance(query, targetString);
		
		return (jaroWinklerSimilarity+(levensteinSimilarity+ngramSimilarity)/2);
	}
	
	public float[] getSimilarity(String query, String[] targetStrings){
		
		float[] scores = new float[targetStrings.length];
		for(int i=0; i<targetStrings.length; i++){
			scores[i] = getSimilarity(query, targetStrings[i]);
		}
		
		return scores;
	}

	public void test(){
		String wikiOK = "Wikipedia is a collaboratively edited, multilingual, free Internet encyclopedia supported by the non-profit Wikimedia Foundation";
		String wikiKO = "wiki is a collaboratively edited, unilangual, pay Internet encyclopedia supported by the non-profit Wikimedia Foundation";
		String wikiOKRevert = "Internet encyclopedia supported by the non-profit Wikimedia Foundation Wikipedia is a collaboratively edited, multilingual, free ";

//		String wikiOK = "Is University of Pennsylvania, Philadelphia very costly?";
//		String wikiKO = "Cost rise in Pennsylvania state University City";
//		String wikiOKRevert = "University of Michigen competes with ivy leagues";

		
		LevensteinDistance levenstein = new LevensteinDistance();
		NGramDistance nGram = new NGramDistance();
		JaroWinklerDistance jaroWinkler = new JaroWinklerDistance();

		float simlOkKo = levenstein.getDistance(wikiOK, wikiKO);
		float simnOkKo = nGram.getDistance(wikiOK, wikiKO);
		float simjOkKo = jaroWinkler.getDistance(wikiOK, wikiKO);

		System.out.println("==wikiOK - wikiKO ==");
		System.out.println("levenstein: " + simlOkKo);
		System.out.println("nGram: " + simnOkKo);
		System.out.println("simjOkKo: " + simjOkKo);
		System.out.println("score: " + (simjOkKo+(simlOkKo+simnOkKo)/2));
		System.out.println();
		// ////////////////////////////////////////////////////

		float simlrevert = levenstein.getDistance(wikiOK, wikiOKRevert);
		float simnrevert = nGram.getDistance(wikiOK, wikiOKRevert);
		float simjrevert = jaroWinkler.getDistance(wikiOK, wikiOKRevert);

		System.out.println("==wikiOK - wikiOKRevert ==");
		System.out.println("levenstein: " + simlrevert);
		System.out.println("nGram: " + simnrevert);
		System.out.println("simjOkKo: " + simjrevert);
		System.out.println("score: " + (simjrevert+(simlrevert+simnrevert)/2));
	}
	
	public static void main(String[] args) {

		String query = "Is University of Pennsylvania, Philadelphia very costly?";
		String target1 = "Cost rise in Pennsylvania state University City";
		String target2 = "University of Michigen competes with ivy leagues";

		TextSimilarity textSimilarity = new TextSimilarity();
		float score1 = textSimilarity.getSimilarity(query, target1);
		float score2 = textSimilarity.getSimilarity(query, target2);
		
		System.out.println("score1: "+score1 +", score2: "+score2 );
	}

}