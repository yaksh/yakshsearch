package edu.upenn.cis455.SearchEngine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Random;

/**
 * @author cis455
 *
 */
public class BTreeDB {
	
	private String directoryPath;
	private DBWrapper BTreeMap;

	public BTreeDB(String directoryPath) {
		if(directoryPath!=null && directoryPath != ""){
			if(!directoryPath.endsWith("/"))
				directoryPath+="/";		
		}
		this.directoryPath = directoryPath;
		BTreeMap = new DBWrapper(directoryPath);
	}

	public void add(String filePath) {
		File fin = new File(filePath);
		add(fin);
	}
	
	public void add(File fin) {
		
		this.BTreeMap.open();
		
		FileInputStream fis;
		BufferedReader br = null;
		
		File fout;
		FileOutputStream fos;
		BufferedWriter bw = null;
		try {
			fis = new FileInputStream(fin);
			br = new BufferedReader(new InputStreamReader(fis));

			// word	<tab> url1;;0,3,5||url4;;20,31,57||url57;;0,3,5
			String line = null;
			String word;
			String cacheUrlString;
			String[] keyVal;
			String[] urlWordFreqArray;
			String[] urlWordFreq;
			String url;
			String[] wordLocationsStrings;
			ArrayList<Integer> wordLocations = new ArrayList<Integer>();
			ArrayList<CacheUrl> cacheUrlList;
			StringBuffer UrlBuffer;
			LeafEntity leaf;
			while((line = br.readLine()) != null){
				// each line is one leaf
				// word	<tab> url1;;0,3,5||url4;;20,31,57||url57;;0,3,5

				try{
					if(line.contains("\t")){
						keyVal = line.split("\t");
						word = keyVal[0];
						cacheUrlString = keyVal[1];
						System.out.println("("+word+", "+cacheUrlString+")");
						
						// create a leaf and add to BTree
						cacheUrlList = new ArrayList<CacheUrl>();
						
						// what if only one URL is there. No pipes. [e.g. word <tab> url1{{0,4,5]
						UrlBuffer = new StringBuffer();
						if(cacheUrlString.contains("||")){
							urlWordFreqArray = cacheUrlString.split("\\|\\|");
							if(urlWordFreqArray != null && urlWordFreqArray.length > 0){
								for(int i=0; i<urlWordFreqArray.length; i++){
									try{
										urlWordFreq = urlWordFreqArray[i].split("\\{\\{");
										if(urlWordFreq.length > 1){
											url = urlWordFreq[0];
											wordLocationsStrings = urlWordFreq[1].split(",");
											for (int k=0; k<wordLocationsStrings.length; k++){
												try{
													Integer loc = Integer.parseInt(wordLocationsStrings[k].trim());
													wordLocations.add(loc);
												}
												catch(Exception e){}
											}
											
											//save in BTree (PR- not known yet)
											double urlPR = new Random().nextDouble()*10;
											UrlBuffer.append(url+"\t"+urlPR+"\n");
											cacheUrlList.add(new CacheUrl(
													url, 
													urlPR, 
													wordLocationsStrings.length, 
													wordLocations));
										}
									}
									catch(Exception e){}
								}
							}
						}
						
						leaf = new LeafEntity(word, this.directoryPath+word+".txt", cacheUrlList);
						this.BTreeMap.put(leaf);
						//write to file
						fout = new File(this.directoryPath+word+".txt");
						fos = new FileOutputStream(fout);
						bw = new BufferedWriter(new OutputStreamWriter(fos));
						bw.write(UrlBuffer.toString());
						bw.close();
					}
				}catch(Exception ex){
					System.err.println("Error parsing the word file");
					ex.printStackTrace();
				}
				
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.BTreeMap.close();
		}
	}
	
	public ArrayList<CacheUrl> get(String word){
		return this.BTreeMap.get(word).getCacheUrlList();
	}
	

	public static void main(String[] args) {

		// one time
		BTreeDB bTree = new BTreeDB("/home/cis455/workspace/BTreeStore");
		bTree.add("/home/cis455/workspace/IndexedStore/test.txt");
		
		bTree.BTreeMap.open();
		LeafEntity appleLeaf = bTree.BTreeMap.get("apple");
		System.out.println(appleLeaf.getFilePath());
		System.out.println(appleLeaf.getCacheUrlList().size());
		for(CacheUrl url : appleLeaf.getCacheUrlList()){
			System.out.println(url.toString()+"\t"+url.getPR()+"\t"+url.getTF());
		}
		//don't forget to close the DB once you're done
		bTree.BTreeMap.close();	
		
	}
}
