package edu.upenn.cis455.SearchEngine;

import java.io.File;
import org.apache.log4j.Logger;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

public class DBWrapper{
	
	static final Logger logger = Logger.getLogger(DBWrapper.class);
	private static final String STORE_NAME = "YAKSH_STORE";
	private static String envDirectory = null;
	private static Environment myEnv;
	private static EntityStore store;

	private PrimaryIndex<String, LeafEntity> leafPrimaryIndex;

	
	/* Write object store wrapper for BerkeleyDB */
	public DBWrapper(String envDirectory){
		this.envDirectory = envDirectory;
	}
	

	public void open(){

		try {
			// create configuration for DB environment
			// create if not already exists
			EnvironmentConfig myEnvConfig = new EnvironmentConfig();
			StoreConfig storeConfig = new StoreConfig();
			
			// If the environment is opened for write, then we want to be
			// able to create the environment if it does not exist.
			myEnvConfig.setAllowCreate(true);
			storeConfig.setAllowCreate(true);
			
			// open/create the DB environment
			File db = new File(this.envDirectory);
			db.setWritable(true, false);
			db.setExecutable(true, false);
			db.setReadable(true, false);
			
			if (!db.exists()){
				System.out.println("Creating DB for the first time");
				db.mkdir();
			}
			
			this.myEnv = new Environment(db, myEnvConfig);
			this.store = new EntityStore(this.myEnv, "EntityStore", storeConfig);
			leafPrimaryIndex = store.getPrimaryIndex(String.class, LeafEntity.class);

		 
		} catch (DatabaseException dbe) {
			logger.info("[" + this.getClass().getSimpleName() + "]: "
					+ "Error opening environment and store: " + dbe.getMessage());
		}
	}
	
	public void close(){
		
		if (this.store != null) {
			try {
				this.store.close();
			} catch(DatabaseException dbe) {
				logger.info("[" + this.getClass().getSimpleName() + "]: "
						+ "Error closing store: " + dbe.getMessage());
			}
		}
		
		try {
			if (this.myEnv != null){
				this.myEnv.cleanLog();
				this.myEnv.close();
			}
		} catch (DatabaseException dbe) {
			logger.info("[" + this.getClass().getSimpleName() + "]: "
					+ "Error closing environment: " + dbe.getMessage());
		}
			
	}
	
	public void sync() {
		if (this.store != null)
			this.store.sync();
		if (this.myEnv != null)
			this.myEnv.sync();
	}

	
	// Getter methods
	public Environment getEnv() {
		return this.myEnv;
	}
	
	public EntityStore getStore() {
		return this.store;
	}
	
	// Other methods
	public void put(LeafEntity leaf) {
		if(this.contains(leaf)){
			this.remove(leaf.getWord());
		}
		leafPrimaryIndex.put(leaf);
	}

	public LeafEntity get(String word) {
		return leafPrimaryIndex.get(word);
	}
	
	public boolean contains(LeafEntity leaf) {
		boolean status = false;
		try{
			if(leafPrimaryIndex.get(leaf.getWord()) != null)
				status = true;
		}catch(Exception ex){
			
		}
		return status;	
	}

	public void remove(String word) {
		leafPrimaryIndex.delete(word);
	}
	
}
