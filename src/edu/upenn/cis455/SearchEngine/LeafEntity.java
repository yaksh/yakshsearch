/**
 * 
 */
package edu.upenn.cis455.SearchEngine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

import static com.sleepycat.persist.model.Relationship.*;

import com.sleepycat.persist.model.SecondaryKey;

/**
 * @author Jatin Sharma
 *
 */

@Entity
public class LeafEntity implements Serializable{
	
	@PrimaryKey
	private String word;
	private String filePath;
	private ArrayList<CacheUrl> cacheUrlList;
	private int IDF;

	
	public LeafEntity(){
		this.cacheUrlList = new ArrayList<CacheUrl>();
	}
	
	public LeafEntity(String word, String filePath, ArrayList<CacheUrl> cacheUrlList){
		this.word =  word;
		this.filePath = filePath;
		// sort the list on PageRank 
		Collections.sort(cacheUrlList);
		this.cacheUrlList = cacheUrlList;
		this.IDF = cacheUrlList.size();
	}

	public int getIDF(){
		return IDF;
	}
	
	public String getWord() {
		return word;
	}
	
	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath
	 *            the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return the cacheUrlList
	 */
	public ArrayList<CacheUrl> getCacheUrlList() {
		return cacheUrlList;
	}

	/**
	 * @param cacheUrlList
	 *            the cacheUrlList to set
	 */
	public void setCacheUrlList(ArrayList<CacheUrl> cacheUrlList) {
		this.cacheUrlList = cacheUrlList;
	}
}

