package edu.upenn.cis455.SearchEngine;

import org.apache.commons.collections15.Transformer;
import Jama.Matrix;

public class JaccardSimilarity extends AbstractSimilarity {

	@Override
	protected double computeSimilarity(Matrix source, Matrix target) {
		double intersection = 0.0D;
		for (int i = 0; i < source.getRowDimension(); i++) {
			intersection += Math.min(source.get(i, 0), target.get(i, 0));
		}
		if (intersection > 0.0D) {
			double union = source.norm1() + target.norm1() - intersection;
			return intersection / union;
		} else {
			return 0.0D;
		}
	}
}
