package edu.upenn.cis455.QuickyEngine;

public class SentenceList implements Comparable {
	
	String rawSentence = "", stopRemovedSentence = "";
	private double weight;
	
	public SentenceList(String sentence) {
		rawSentence = new String(sentence);
		weight = 0.0;	
	}

	@Override
	public int compareTo(Object arg0) {
		double otherWeight = ((SentenceList)arg0).getWeight();
		return otherWeight > this.getWeight() ? 1 : -1;
	} 

	public void setRawSentense(String sentence) {
		rawSentence = new String(sentence);      	
	}
 
	public void setStopsRemovedSentence(String rawSentence) {
		stopRemovedSentence = new String(rawSentence);
	}  

	public String getRawSentence() {
		return rawSentence;
	}   
	
	public String getStopwordsRemovedSentence() {
		return stopRemovedSentence;
	}

	public void setWeight(double wg) {
		weight = weight + wg;
	} 
	
	public double getWeight() { 
		return weight; 
	}
}