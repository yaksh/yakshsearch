package edu.upenn.cis455.QuickyEngine; 

import java.io.*;
import java.util.StringTokenizer;
import java.util.AbstractList;

public class Sentense {
	
	private String document = "";
	
	public Sentense(String content) {
		this.document = content.replaceAll(" +", " ")
				.replaceAll("\\.", "\\. ")
				.replaceAll(" \\.", "\\. ")
				.replaceAll("\r\n", "\\. ");
	}
	
	public Sentense(File fp) {
		int sz; 
		byte bt[];  
		try { 			            
			FileInputStream fis = new FileInputStream(fp);
			sz = (int)fp.length();	
			bt = new byte[sz];
			fis.read(bt);            
			this.document = new String(bt).replaceAll(" +", " ")
					.replaceAll("\\. ", "\\.")
					.replaceAll(" \\.", "\\.")
					.replaceAll("\\.", "\\. ");
			
//			.replaceAll("\r\n", "\\.")
					   	
		} 
		catch(IOException ex){
			System.out.println(ex);
		}
	 }


	public AbstractList separateSentense(AbstractList arrayList) {  
		
		int fs1 = 0, fs2 = 0;    	  
		int next = 0;
	    while (next < (document.length()-1)){
	        
	    	next = document.indexOf(".", fs2);
			if(next == -1){	 	
				break;	
			} else if(next == document.lastIndexOf(".")){
				String str=(document.substring(fs1, next+1)); 
				arrayList.add(new SentenceList(str));		
				fs2 = next+1;  
				fs1 = fs2;	  
				break;
			} else if(document.charAt(next+1) == ' ' || document.charAt(next+1) == '\r' || document.charAt(next+1) =='\n'){		        	     				
				String str=(document.substring(fs1, next+1)).trim();		    
				arrayList.add(new SentenceList(str));			
				fs2 = next+1; 	            
				fs1 = fs2;  			
			} else{
				fs2 = next+1;  	   	  		                   
			}
	    }
	     return arrayList;	
	}

}