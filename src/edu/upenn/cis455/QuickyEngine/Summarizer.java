package edu.upenn.cis455.QuickyEngine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Enumeration;
import java.util.Set;

import opennlp.tools.stemmer.PorterStemmer;

import org.jsoup.Jsoup;
import org.jsoup.examples.HtmlToPlainText;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Summarizer{

	private ArrayList<SentenceList> arrayList;
	private Hashtable<String, WordList> hashtable;
	private double sentenceCount;
	private Stopword stopWords;
	private Special specialWords;
	private Sentense sentense;
	private PorterStemmer stemmer;
  
	
	public Summarizer(Object obj){	
		this.arrayList = new ArrayList<SentenceList>();
		this.hashtable = new Hashtable<String, WordList>();
		this.stopWords = new Stopword();
	    this.specialWords = new Special();
	    this.stemmer = new PorterStemmer();
	    
		if(obj instanceof File){
			this.sentense = new Sentense((File)obj);
		} else if (obj instanceof String){
			this.sentense = new Sentense((String)obj);
		}
	}
	
	public Summarizer(Object obj, boolean isHtml){	
		this.arrayList = new ArrayList<SentenceList>();
		this.hashtable = new Hashtable<String, WordList>();
		this.stopWords = new Stopword();
	    this.specialWords = new Special();
	    this.stemmer = new PorterStemmer();
	    
	    String documentString = null;
		if(obj instanceof File){
			int sz; 
			byte bt[];  
			try { 			            
				FileInputStream fis = new FileInputStream((File)obj);
				sz = (int)((File)obj).length();	
				bt = new byte[sz];
				fis.read(bt);            
				documentString = new String(bt);    	
			} 
			catch(IOException ex){
				System.out.println(ex);
			}
		} else if (obj instanceof String){
			documentString = (String)obj;
		}
		if(isHtml){
			documentString = Jsoup.parse(documentString).select("p").text();
	    }
		this.sentense = new Sentense((String)documentString);
	}
	
	public Summarizer(String url, String docType){	
		this.arrayList = new ArrayList<SentenceList>();
		this.hashtable = new Hashtable<String, WordList>();
		this.stopWords = new Stopword();
	    this.specialWords = new Special();
	    this.stemmer = new PorterStemmer();

	    String documentString = null;
	    if(docType.toLowerCase().equals("url")){
	    	Document doc = null;
			try {
				doc = Jsoup.connect(url).get();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Elements ps = doc.select("p");
			documentString = ps.text();  
	    }
		this.sentense = new Sentense((String)documentString);
	}
	
	
	public void separateSentense(){	
		this.sentense.separateSentense(arrayList);
		sentenceCount = arrayList.size();
	}
	
	public void removeStopWords(){	
	    for(int i = 0; i < arrayList.size(); i++) {    
			SentenceList sl = (SentenceList)arrayList.get(i);    
			sl.setStopsRemovedSentence(specialWords.remove(sl.getRawSentence()));
			sl.setStopsRemovedSentence(stopWords.remove(sl.getStopwordsRemovedSentence()));
	    } 	
	}
	
	public void uniqueWords(){     	        
		for(int i = 0; i<arrayList.size(); i++){    
			SentenceList sl = (SentenceList)arrayList.get(i);    			
			String sen = sl.getStopwordsRemovedSentence();	
			int wc = 0;
			StringTokenizer stk = new StringTokenizer(sen, " ");

			while (stk.hasMoreElements()){			   
				String tok=(String)stk.nextElement();
				tok = this.stemmer.stem(tok.trim()
						.replaceAll("'re","")
						.replaceAll("'s", "")
						.replaceAll("[^a-zA-Z ]", "").toLowerCase()); 
				wc++;	   	
				if(!hashtable.containsKey(tok) && tok.length() >=3){
					addword(tok,i,wc);
				}else if(hashtable.containsKey(tok)){
					upword(tok,i,wc);	        
				}
			}

		}
		Enumeration key=hashtable.keys();
//		System.out.println(hashtable.size());
		while (key.hasMoreElements() ) {
			WordList wl=(WordList)hashtable.get(key.nextElement());	 
//			System.out.println(wl.getword());
		}  
	}
	
	public void stemming(){     	        

		int sz = hashtable.size();
		double[][] wdis = new double[sz][sz];
		Set s1 = hashtable.keySet();
		Object obj[] = s1.toArray();   
		  
		for(int i=0;i<sz;i++) {        
			String str1=(String) obj[i];
			for(int j=0;j<sz;j++) {
				String str2=(String) obj[j];
				if(i!=j)
				wdis[i][j] = difpos(str1,str2);		
			}
		} 
	
		for(int i=0;i<sz;i++) {        
			String str1=(String)obj[i];
			for(int j=0;j<sz;j++) {
				String str2 = (String)obj[j];	
				if(i!=j && wdis[i][j] >= 3.0 ) {              
					stemword(str1,str2); 		
				}	  			
			}
		} 	
		
	}
		
	public void stemword(String w1, String w2){

		if(!hashtable.containsKey(w2) || !hashtable.containsKey(w1))
			return; 

		WordList wl1 = (WordList)hashtable.remove(w1);
		WordList wl2 = (WordList)hashtable.remove(w2);
		ArrayList wp = wl2.getWordPosition();
		ArrayList stopWords = wl2.getSentensePosition();
	  
		for(int i=0; i<wp.size(); i++){
			String wp2=(String)wp.get(i); 
			String sp2=(String)stopWords.get(i); 	
			wl1.incrementCount(Integer.parseInt(wp2),Integer.parseInt(sp2));
		}
		hashtable.put(w1,wl1);       
	}

	public WordList significant(){

		Enumeration key = hashtable.keys();
		WordList wl = null;
		while (key.hasMoreElements() ) {
			wl = (WordList)hashtable.get(key.nextElement());	      	
		} 
		
		return wl;
	}


	public void weight(){    
		Enumeration key = hashtable.keys();	    
		while (key.hasMoreElements())     
			setweight(key.nextElement());	    	       
	}


	public void ranking() {
		
		SentenceList  sl = null;  
		double max = 0.0;
		int mi = 0;

		for(int i=0; i<arrayList.size(); i++){ 	
			sl = (SentenceList)arrayList.get(i);    			
			String sen = sl.getStopwordsRemovedSentence();	
			Enumeration key = hashtable.keys();
			
			while(key.hasMoreElements()){	
				String str=(String)key.nextElement();         	
				if(sen.indexOf(str) != -1 ){
					WordList wl=(WordList)hashtable.get(str); 
					sl.setWeight(wl.getWeight());      		
				} 	       	
			}
		}	

		for(int i=0;i<arrayList.size();i++) { 	
			sl=(SentenceList)arrayList.get(i);    			
			if(sl.getWeight() > max){
				max =  sl.getWeight();
				mi=i;
			}
		}
		
		for(int i=0; i < arrayList.size(); i++) { 
			SentenceList s = (SentenceList)arrayList.get(i); 
//			System.out.println("{"+s.weight()+"}"+s.getrawsentense());
		}  

	}
	
	String digest(int summarizationLength){
		ArrayList<SentenceList> rankedList = new ArrayList<SentenceList>();
		rankedList.addAll(arrayList);
		HashMap<SentenceList, Integer> rankedMap = new HashMap<SentenceList, Integer>();
		for(int i=0; i< rankedList.size(); i++){
			rankedMap.put(rankedList.get(i), i);
		}
		
		Collections.sort(rankedList);
		SentenceList sthreshold = null;
		if(summarizationLength <= rankedList.size())
			sthreshold = (SentenceList)rankedList.get(summarizationLength - 1); 
		else
			sthreshold = (SentenceList)rankedList.get(rankedList.size() - 1);
		
		StringBuffer buffer = new StringBuffer();
		int count = 0;
		for(int i=0; i < rankedList.size(); i++) { 
			SentenceList s = (SentenceList)rankedList.get(i); 
			if(s.getWeight() >= sthreshold.getWeight() && count < summarizationLength){
//				buffer.append(s.getRawSentence() + "{"+s.getWeight()+"}...");   
//				buffer.append(s.getRawSentence() + "..."); 
				buffer.append(s.getRawSentence() + "\n"); 
				count++;
			}
		}  
		return buffer.toString();
	}

	
	/* Helper Methods */
	public double difpos(String str1,String str2){     
		int s1=str1.length();
		int s2=str2.length(); 
		int sz=(s1 > s2 ) ? s1 : s2;
		int mz=(s1 < s2 ) ? s1 : s2;
		double dp=mz,sm=0;  
		for(int i=0; i<mz; i++){
			if( str1.charAt(i) != str2.charAt(i) ){
				dp=i+1;
				break;
			}
			else{
				sm++;
			}
		}   
		return(sm*(dp/sz));             
	}

	private void addword(String tok,int stopWords,int wp){
	    WordList wl=new WordList(tok);
	    wl.incrementCount(stopWords+1,wp);    
	    hashtable.put(tok,wl);
	}
	
	private void upword(String tok,int stopWords,int wp){    
	     WordList wl=(WordList)hashtable.remove(tok);
	     wl.incrementCount(stopWords+1,wp);
	     hashtable.put(tok,wl);     		
	}

	private void delword(String tok){    
	     WordList wl=(WordList)hashtable.remove(tok);
	     if(wl.getcount() > 3)
	         hashtable.put(tok, wl);          
	}

	private void setweight(Object tok){    
	     double wg = 0.0;
	     WordList wl = (WordList)hashtable.get(tok);
	     double tf = wl.getcount();
	     double df = wl.getSentenseCount(); 
	     wg = tf*Math.log10(sentenceCount/df);         
	     wl.setWeight(wg);         	      
	}
	

	public Summary summarize(int summarizationLength){
		
		Summary summary = new Summary();
		
		separateSentense();	
		removeStopWords();
		uniqueWords();		
		stemming();
		significant();	
		weight();	
		ranking();
		summary.summary = digest(summarizationLength);
		summary.wordListMap = this.hashtable;
		
		return summary;
	}


	public static void main(String str[]){
		
		String documentString = "Rajeev Motwani was born in Jammu and grew up in New Delhi. "
				+ "His father was in the Indian Army. He has two brothers. As a child, "
				+ "inspired by luminaries like Gauss, he wanted to become a mathematician. "
				+ "Motwani went to St Columba's School, New Delhi. He completed his B.Tech in "
				+ "Computer Science from the Indian Institute of Technology Kanpur in 1983 and "
				+ "got his Ph.D. in Computer Science from the University of California, Berkeley "
				+ "in 1988 under the supervision of Richard M. Karp.Motwani joined Stanford soon "
				+ "after U.C. Berkeley. He founded the Mining Data at Stanford project (MIDAS), "
				+ "an umbrella organization for several groups looking into new and innovative "
				+ "data management concepts. His research included data privacy, web search, robotics, "
				+ "and computational drug design. He is also one of the originators of the "
				+ "Locality-sensitive hashing algorithm.Motwani was one of the co-authors "
				+ "(with Larry Page and Sergey Brin, and Terry Winograd) of an influential early "
				+ "paper on the PageRank algorithm. He also co-authored another seminal search "
				+ "paper What Can You Do With A Web In Your Pocket with those same authors.[6] "
				+ "PageRank was the basis for search techniques of Google (founded by Page and Brin), "
				+ "and Motwani advised or taught many of Google's developers and researchers,[7] "
				+ "including the first employee, Craig Silverstein. He was an author of two widely "
				+ "used theoretical computer science textbooks: Randomized Algorithms with Prabhakar "
				+ "Raghavan[9] and Introduction to Automata Theory, Languages, and Computation with "
				+ "John Hopcroft and Jeffrey Ullman. He was an avid angel investor and helped fund "
				+ "a number of startups to emerge from Stanford. He sat on boards including Google, "
				+ "Kaboodle, Mimosa Systems (acquired by Iron Mountain Incorporated), Adchemy, "
				+ "Baynote, Vuclip, NeoPath Networks (acquired by Cisco Systems in 2007), "
				+ "Tapulous and Stanford Student Enterprises. He was active in the Business "
				+ "Association of Stanford Entrepreneurial Students (BASES).[11][12][13] He was a "
				+ "winner of the Gödel Prize in 2001 for his work on the PCP theorem and its "
				+ "applications to hardness of approximation.[14][15] He served on the editorial "
				+ "boards of SIAM Journal on Computing, Journal of Computer and System Sciences, "
				+ "ACM Transactions on Knowledge Discovery from Data, and IEEE Transactions on "
				+ "Knowledge and Data Engineering.";
		
		// Type1
		Summarizer summarizer = new Summarizer(documentString);
		// Type2
		//Summarizer summarizer = new Summarizer(new File("models/file3.html"), true);
		// Type3
		//Summarizer summarizer = new Summarizer("http://www.networkworld.com/article/2873964/sdn/the-first-place-to-tackle-sdn-in-the-wan.html", "url");
		// Type4
//		String url = "https://en.wikipedia.org/wiki/Special:Search?search=";
//		String keywords = "Kanwal Rekhi";
//		Summarizer summarizer = new Summarizer(url+keywords.replace(" ","+"), "url");

		int summarizationLength = 5;
		Summary summary = summarizer.summarize(summarizationLength);	
		
//		System.out.println(summary.toString());

		
//		Enumeration key=summary.wordListMap.keys();
//		Set<String> keys =summary.wordListMap.keySet();
//		List<String> keyList = new ArrayList<String>();
//		keyList.addAll(keys);
//		Collections.sort(keyList);
//		System.out.println(summary.wordListMap.size());
//		for (String key : keyList) {
//			WordList wl=(WordList)summary.wordListMap.get(key);	 
//			System.out.println("word:"+wl.getword()+",\t count:"+ wl.getcount()+",\t weight: "+ wl.getWeight());
//		}
		
		Enumeration keys = summary.wordListMap.keys();
		System.out.println(summary.wordListMap.size());
		while (keys.hasMoreElements()) {
			WordList wl=(WordList)summary.wordListMap.get(keys.nextElement());	 
			System.out.println("word:"+wl.getword()+
							",\t count:"+ wl.getcount()+
							",\t weight: "+ wl.getWeight()+ 
							",\t sentence Position:"+ Arrays.toString(wl.getSentensePosition().toArray())+
							",\t word Position:"+ Arrays.toString(wl.getWordPosition().toArray())+
							",\t weight: "+ wl.getWeight());
			
		
		}
		
	}   

}