/**
 * 
 */
package edu.upenn.cis455.QuickyEngine;

import java.util.Hashtable;

/**
 * @author cis455
 *
 */
public class Summary {
	public String summary;
	public Hashtable<String, WordList> wordListMap;
	
	@Override
	public String toString(){
		return summary;
	}
	
}
