package edu.upenn.cis455.QueryEngine;

/**
 * @author Jatin Sharma
 */

abstract public class QueryParser {
	
    //This method defines a generic structure for parsing the query
    abstract public String[] getKeywords(String searchQuery);
}

