/**
 * 
 */
package edu.upenn.cis455.QueryEngine;

import simplenlg.framework.*;
import simplenlg.lexicon.*;
import simplenlg.realiser.english.*;
import simplenlg.phrasespec.*;
import simplenlg.features.*;

/**
 * @author Jatin Sharma
 *
 */

public class AutoCorrector {

        public static void main(String[] args) {
                Lexicon lexicon = Lexicon.getDefaultLexicon();
                NLGFactory nlgFactory = new NLGFactory(lexicon);
                Realiser realiser = new Realiser(lexicon);
                
                NLGElement sentence = nlgFactory.createSentence("my dog is happy");
				String output = realiser.realiseSentence(sentence);
				System.out.println(output);
				
				sentence = nlgFactory.createSentence("where going");
				output = realiser.realiseSentence(sentence);
				System.out.println(output);
				
				sentence = nlgFactory.createSentence("find gas stations");
				output = realiser.realiseSentence(sentence);
				System.out.println(output);
				
				sentence = nlgFactory.createSentence("where live prince charles");
				output = realiser.realiseSentence(sentence);
				System.out.println(output);
				
				sentence= nlgFactory.createSentence("what is your major");
				output = realiser.realiseSentence(sentence);
				System.out.println(output);

        }

}
