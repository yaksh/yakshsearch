package edu.upenn.cis455.QueryEngine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeSet;

import opennlp.tools.cmdline.PerformanceMonitor;
import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.lemmatizer.DictionaryLemmatizer;
import opennlp.tools.lemmatizer.SimpleLemmatizer;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSSample;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.stemmer.PorterStemmer;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.tokenize.WhitespaceTokenizer;
import opennlp.tools.util.InvalidFormatException;
import opennlp.tools.util.ObjectStream;
import opennlp.tools.util.PlainTextByLineStream;
import opennlp.tools.util.Span;

/**
 * @author Jatin Sharma
 *
 */
public class NLPQueryParser extends QueryParser {

	private Tokenizer tokenizer;
	private SentenceDetectorME sentenceDetector;
	private NameFinderME personFinder, orgFinder, locationFinder;
	private PorterStemmer stemmer;
	private SimpleLemmatizer lemmatizer;
	private POSTaggerME tagger;
	private TreeSet<String> stopwords = new TreeSet<String>();
	private final String englishStopWords[] = {
			 "a", "an", "and", "are", "as", "at", "be", "but", "by",
			 "for", "if", "in", "into", "is", "it",
			 "no", "not", "of", "on", "or", "such",
			 "that", "the", "their", "then", "there", "these",
			 "they", "this", "to", "was", "will", "with"
			 };
	
	public NLPQueryParser(){
		loadTokenizer();
		loadSentenseDetector();
		loadNameFinder();
		loadPorterStemmer();
		loadLemmatizer();
		loadPOSTagger();
		loadStopWordsCleaner();
	}
	
	// not in use anymore
	private void loadTokenizer(){
		// always start with a model, a model is learned from training data
		InputStream is;
		try {
			is = new FileInputStream("models/en-token.bin");
			TokenizerModel model = new TokenizerModel(is);
			this.tokenizer = new TokenizerME(model);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void loadSentenseDetector(){

		// always start with a model, a model is learned from training data
		InputStream is = null;
		try {
			is = new FileInputStream("models/en-sent.bin");
			SentenceModel model = new SentenceModel(is);
			this.sentenceDetector = new SentenceDetectorME(model);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				is.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private void loadNameFinder(){

		// always start with a model, a model is learned from training data
		InputStream is1 = null, is2 = null, is3 = null;
		try {
			is1 = new FileInputStream("models/en-ner-person.bin");
			is2 = new FileInputStream("models/en-ner-organization.bin");
			is3 = new FileInputStream("models/en-ner-location.bin");
			TokenNameFinderModel personModel = new TokenNameFinderModel(is1);
			TokenNameFinderModel orgModel = new TokenNameFinderModel(is2);
			TokenNameFinderModel locationModel = new TokenNameFinderModel(is3);
			this.personFinder = new NameFinderME(personModel);
			this.orgFinder = new NameFinderME(orgModel);
			this.locationFinder = new NameFinderME(locationModel);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				is1.close();
				is2.close();
				is3.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private void loadLemmatizer(){

		// always start with a model, a model is learned from training data
		InputStream is = null;
		try {
			is = new FileInputStream("models/en-lemmatizer.dict");
			this.lemmatizer = new SimpleLemmatizer(is);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				is.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private void loadPOSTagger(){

		POSModel model = new POSModelLoader().load(new File("models/en-pos-maxent.bin"));
		this.tagger = new POSTaggerME(model);
	}
	
	private void loadPorterStemmer(){
		this.stemmer = new PorterStemmer();
	}
	
	private void loadStopWordsCleaner(){
		for (int i=0; i< englishStopWords.length;i++)
			this.stopwords.add(englishStopWords[i]);
	}
	

	public String[] getSentences(String paragraph) {
		return this.sentenceDetector.sentDetect(paragraph);
	}
	
	String[] getTokens(String searchString) {
		return searchString.replaceAll("[^a-zA-Z ]", "").toLowerCase().split("\\s+");
	}
	
	public String[] getNameEntities(String searchString) {

		String[] tokens = searchString.replaceAll("[^a-zA-Z ]", "").split("\\s+");
//		String tokens[] = WhitespaceTokenizer.INSTANCE.tokenize(searchString);
		String[] persons = Span.spansToStrings(this.personFinder.find(tokens), tokens);
		String[] orgs = Span.spansToStrings(this.orgFinder.find(tokens), tokens);
		String[] locations = Span.spansToStrings(this.locationFinder.find(tokens), tokens);
		
		List<String> namesList = new ArrayList<String>(persons.length+orgs.length+locations.length);
	    Collections.addAll(namesList, persons);
	    Collections.addAll(namesList, orgs);
	    Collections.addAll(namesList, locations);
	    return namesList.toArray(new String[namesList.size()]);

	}
	

	
	//This first removes all non-letter characters, folds to lowercase, 
	// then splits the input, doing all the work in a single line:
	String[] normalize(String searchString){
		return searchString.replaceAll("[^a-zA-Z ]", "").toLowerCase().split("\\s+");
	}

	public String[] getPOSTags(String[] searchTokens){
		String[] tags = this.tagger.tag(searchTokens);
		return tags;
	}
	
	String[] removeStopWords(String[] searchKeywords) {
		ArrayList<String> newTokens = new ArrayList<String>(Arrays.asList(searchKeywords));
		for (int i=0;i<newTokens.size();i++){
			if (this.stopwords.contains(newTokens.get(i)))
				newTokens.remove(i);
		}
		return (String[])newTokens.toArray(new String[newTokens.size()]);
	}

	String[] spellCorrect(String[] searchKeywords) {
		// TODO Auto-generated method stub
		return searchKeywords;
	}


	public String[] getStems(String[] searchKeywords){
		String[] searchStems = new String[searchKeywords.length];
		for(int i=0; i<searchKeywords.length; i++){
			searchStems[i] = this.stemmer.stem(searchKeywords[i]); 
		}
		return searchStems;
	}
	
	public String[] getLemmas(String[] searchKeywords, String[] POSTags){
		String[] searchLemmas = new String[searchKeywords.length];
		for(int i=0; i<searchKeywords.length; i++){
			searchLemmas[i] = this.lemmatizer.lemmatize(searchKeywords[i], POSTags[i]);
		}
		return searchLemmas;
	}


	List<String> getSuggestions(String searchQuery) {
		
		// Declare the list 
		List<String> suggestedQueryList = new ArrayList<String>();
		
		// TODO prepare some suggestions and add to the list
		// suggestedQueryList.add(suggestion);
		
		return suggestedQueryList;
	}
	
	@Override
	public String[] getKeywords(String searchString){
		
		String[] searchNameEntities = getNameEntities(searchString);
		String[] searchTokens = getTokens(searchString);
		String[] searchTagsNoStopWords = removeStopWords(searchTokens);
		String[] searchStems = getStems(searchTagsNoStopWords);
		
		String[] searchTags = getPOSTags(searchTagsNoStopWords);
		String[] searchLemmas = getLemmas(searchTagsNoStopWords, searchTags);

		return searchStems;
	}
	
	public static void main(String[] args){
		
		NLPQueryParser parser = new NLPQueryParser();
		String searchString = "how to do your first operation by John McCaley, Google Inc, San Jose?";
		
		String[] searchNameEntities = parser.getNameEntities(searchString);
		String[] searchTokens = parser.getTokens(searchString);
		String[] searchTagsNoStopWords = parser.removeStopWords(searchTokens);
		String[] searchTags = parser.getPOSTags(searchTagsNoStopWords);

		String[] searchStems = parser.getStems(searchTagsNoStopWords);
		String[] searchLemmas = parser.getLemmas(searchTagsNoStopWords, searchTags);
		
		System.out.println("searchString \t: "+searchString);
		System.out.println("searchNamesEntities \t: "+Arrays.toString(searchNameEntities));
		System.out.println("searchTokens \t: "+Arrays.toString(searchTokens));
		System.out.println("searchTagsNoStopWords \t: "+Arrays.toString(searchTagsNoStopWords));
		System.out.println("searchTags \t: "+Arrays.toString(searchTags));
		System.out.println("searchStems \t: "+Arrays.toString(searchStems));
		System.out.println("searchLemmas \t: "+Arrays.toString(searchLemmas));
		
//		Object lock = new Object();
//		String[] searchKeywords = null;
//		long start = System.nanoTime();
//		for(int i=0; i<10; i++){
//			synchronized(lock){
//				searchKeywords = parser.getKeywords(searchString);
//			}
//		}
//		System.out.println("searchKeywords \t: "+Arrays.toString(searchKeywords));
//		System.out.println((System.nanoTime()-start)/(10*1000000) + " ms");
	}


}