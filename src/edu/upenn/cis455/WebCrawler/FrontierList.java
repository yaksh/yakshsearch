package edu.upenn.cis455.WebCrawler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

/**
 * @author Jatin Sharma
 *
 */

public class FrontierList<T> {

	private int size;
	private ArrayList<LinkedList<T>> buckets;
	private int nextBucketNumber;
	
	// constructor
	public FrontierList(){
		this.size = 0;
		this.buckets = new ArrayList<LinkedList<T>>(10);
		this.nextBucketNumber = 0;
		
		// initialize all 10 buckets
		for (int i = 0; i < 10; i++) {
			this.buckets.add(new LinkedList<T>());
		}
	}

	// O(1) 
	public boolean add(T object) {
		
		// pick a bucket
		Random rand = new Random();
		int bucketNumber = rand.nextInt(10);
		
		// add to the randomly chosen bucket
		LinkedList<T> bucket = (LinkedList<T>) this.buckets.get(bucketNumber);
		if(bucket == null){
			bucket = new LinkedList<T>();
		}
		// adds at the end of the list
		boolean status = bucket.add(object);
		if(status)
			this.size++;

		return status;
	}
	
	// O(lengthOfInput) 
	public void addAll(Collection<T> collection) {
		for(T object : collection){
			add(object);
		}
	}

	// O(1) amortized
	public T get() {
		
		// if there is no nextElement yet generate it
		LinkedList<T> bucket = ((LinkedList<T>)this.buckets.get(nextBucketNumber));
		if (bucket == null || this.size == 0) {
			return null;
		}else{
			while(bucket.size() == 0){
				// bucket is empty so go for next bucket
				nextBucketNumber = (nextBucketNumber+1)%10;
				bucket = ((LinkedList<T>)this.buckets.get(nextBucketNumber));
			}
			// finally we have a non-empty bucket return first element w/o removing it
			return bucket.get(0);
		}

	}

	// O(1)
	public boolean isEmpty() {
		if(this.size == 0)
			return true;
		else
			return false;
	}
	
	// O(1) amortized
	public T remove() {
		
		T object = get();
		if(object != null){
			LinkedList<T> bucket = ((LinkedList<T>)this.buckets.get(nextBucketNumber));
			nextBucketNumber = (nextBucketNumber+1)%10;
			bucket.remove(0);
			this.size--;
			return object;
		}else{
			return null;
		}
	}

	// O(1)
	public int size() {
		return this.size;
	}
	
	// O(numBuckets) = O(10) = O(1) 
	public void clear() {
		this.size = 0;
		this.buckets = new ArrayList<LinkedList<T>>(10);
		this.nextBucketNumber = 0;
		
		// initialize all 10 buckets
		for (int i = 0; i < 10; i++) {
			this.buckets.add(new LinkedList<T>());
		}
		// to collected dangling references to original buckets
		System.gc();
	}
	
	// O(numOfElements) needs to be optimized to O(1) using bloomFilter or HashMap
	public boolean contains(T object){
		
		for(LinkedList<T> bucket : this.buckets){
			if(bucket.contains(object))
				return true;
		}
		return false;
	}
	
	// O(sizeOfFrontierList) 
	public File writeToFile(String filePath) {

		File fout = new File(filePath);
		FileOutputStream fos;
		BufferedWriter bw = null;
		try {
			fos = new FileOutputStream(fout);
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			
			for(LinkedList<T> bucket : this.buckets){
				for(T object : bucket){
					bw.write(object.toString());
					bw.newLine();
				}
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return fout;
	}
	
	public static void main(String[] args){
		
		FrontierList<String> frontier = new FrontierList<String>();
		
		System.out.println(frontier.size());
		
		frontier.add("1");
		frontier.add("2");
		frontier.add("3");
		
		List<String> objectList = new ArrayList<String>();
		objectList.add("4");
		objectList.add("5");
		frontier.addAll(objectList);
		
		frontier.writeToFile("models/frontierListDemo.txt");
		
		//1
		System.out.println(frontier.size());
		System.out.println(frontier.get());
		System.out.println(frontier.remove());
		//2
		System.out.println(frontier.size());
		System.out.println(frontier.get());
		System.out.println(frontier.remove());
		//3
		System.out.println(frontier.size());
		System.out.println(frontier.remove());
		frontier.clear();
		
		//4
		System.out.println(frontier.size());
		System.out.println(frontier.remove());
		//5
		System.out.println(frontier.size());
		System.out.println(frontier.remove());
		//6
		System.out.println(frontier.size());
		System.out.println(frontier.remove());
		
	}

}
