package edu.upenn.cis455.PageRanker;

import java.util.Collection;

public interface RankedVector<N> {
    double getWeight(N node);
    Collection<WeightedNode<N>> entriesInOrder();
}