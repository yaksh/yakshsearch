
package edu.upenn.cis455.PageRanker;

import java.util.Collection;

public interface WeightedDirectedGraph<I> {
    Node<I> node(I identifier);
    Collection<Node<I>> allNodes();
}
