/**
 * 
 */
package edu.upenn.cis455.PageRanker;

/**
 * @author cis455
 *
 */

public interface WeightedNode<N> {
    N getNode();
    double getWeight();
}
