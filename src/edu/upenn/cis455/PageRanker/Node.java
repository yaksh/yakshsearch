/**
 * 
 */
package edu.upenn.cis455.PageRanker;

import java.util.Collection;

public interface Node<I> {
    Collection<WeightedEdge<I>> inEdges();
    float totalOutboundEdgeWeight();
    I identifier();
}
