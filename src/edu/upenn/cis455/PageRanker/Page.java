package edu.upenn.cis455.PageRanker;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Jatin Sharma
 *
 */

public class Page {
	String url;
	double weight;
	Page parent;
	HashMap<Page, Double> fanIn;
	ArrayList<Page> fanout;	
	
	public Page(){
		url = "";
		weight = 0.0;
		parent = null;
		fanIn = new HashMap<Page, Double>();
		fanout = new ArrayList<Page>();
	}
}
