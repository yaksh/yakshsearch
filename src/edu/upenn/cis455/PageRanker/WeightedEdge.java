/**
 * 
 */
package edu.upenn.cis455.PageRanker;

/**
 * @author cis455
 *
 */
public interface WeightedEdge<I> {
    Node<I> getHead();
    Node<I> getTail();
    float getWeight();
}
