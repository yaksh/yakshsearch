/**
 * 
 */
package edu.upenn.cis455.Indexer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import edu.upenn.cis455.AWS.S3Storage;
import edu.upenn.cis455.SearchEngine.BTreeDB;

/**
 * @author cis455
 *
 */
public class IndexerTemplate {
	
	public static String awsAccessKey;
	public static String awsSecretKey;
	
	/**
	 * @param bucket
	 * @param folder
	 */
	public void buildIndex(String bucket, String folder){
		BTreeDB db = new BTreeDB("BerkleyDB");
		S3Storage s3 = new S3Storage(IndexerTemplate.awsAccessKey, IndexerTemplate.awsSecretKey);
    	ArrayList<String> outKeys = s3.getFilesInFolder(bucket, folder);
    	for(String key : outKeys){
    		try{
	    		File downloaded = File.createTempFile("index", ".csv");
	    		downloaded.deleteOnExit();
	    		System.out.println("Downloading file " + key);
	    		s3.download(bucket, key, downloaded);
    			System.out.println("Adding file " + key + " to berkley DB");
    			db.add(downloaded);
    		}
    		catch(Exception e){
    			System.err.println("Error processing file " + key);
    			e.printStackTrace();
    		}
    		finally{
    			System.out.println("Done adding file " + key);
    		}
    	}
	}
	
	public static void main(String[] args) {
		IndexerTemplate.awsAccessKey = args[0];
		IndexerTemplate.awsSecretKey = args[1];
		
		IndexerTemplate indexerTemplate = new IndexerTemplate();
		indexerTemplate.buildIndex(args[2], args[3]);
		System.exit(0);
	}
}
